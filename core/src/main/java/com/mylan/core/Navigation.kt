package com.mylan.core

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector


sealed class Navigation(val route: String, val name: String, val icon: ImageVector) {
    data object Home : Navigation("home", "Home", Icons.Default.Home)
    data object Profile : Navigation("profile", "Profile", Icons.Default.Person)
    data object Search : Navigation("search", "Search", Icons.Default.Search)
    data object Settings : Navigation("settings", "Settings", Icons.Default.Settings)
}

val navigations = listOf(
    Navigation.Home,
    Navigation.Profile,
    Navigation.Search,
    Navigation.Settings
)