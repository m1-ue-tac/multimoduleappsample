package com.mylan.myapplication

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.mylan.core.Navigation
import com.mylan.core.navigations
import com.mylan.home.HomeScreen
import com.mylan.search.SearchScreen
import kotlinx.coroutines.launch

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun MainScreen(navController: NavHostController = rememberNavController()) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()

    val actionBarTitle = remember { mutableStateOf(Navigation.Home.name) }

    val context = LocalContext.current
    LaunchedEffect(navController) {
        navController.currentBackStackEntryFlow.collect { backStackEntry ->
            // You can map the title based on the route using:
            navigations.forEach { nav ->
                if (nav.route == backStackEntry.destination.route) {
                    actionBarTitle.value = nav.name
                }
            }
        }
    }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        topBar = {
            TopAppBar(title = { Text(text = actionBarTitle.value) })
        },
        bottomBar = {
            NavigationBar {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentDestination = navBackStackEntry?.destination

                listOf(
                    Navigation.Home,
                    Navigation.Search,
                    Navigation.Profile
                ).forEach { nav ->
                    NavigationBarItem(
                        selected = currentDestination?.hierarchy?.any { it.route == nav.route } == true,
                        onClick = { navController.navigate(nav.route) },
                        label = { Text(text = nav.name) },
                        icon = { Icon(imageVector = nav.icon, contentDescription = nav.name) }
                    )
                }
            }
        }
    ) {
        Surface(
            modifier = Modifier
                .padding(it)
                .fillMaxSize(), color = MaterialTheme.colorScheme.background
        ) {

            NavHost(navController = navController, startDestination = Navigation.Home.route) {

                composable(Navigation.Home.route) {
                    HomeScreen { navController.navigate(Navigation.Settings.route) }
                }

                composable(Navigation.Search.route) {
                    SearchScreen { navController.navigate(Navigation.Settings.route) }
                }

                composable(Navigation.Profile.route) {
                    ProfileScreen {
                        navController.navigate(Navigation.Settings.route)
                    }
                }

                composable(Navigation.Settings.route) {
                    SettingsScreen {
                        scope.launch {
                            snackbarHostState.showSnackbar("Welcome to ${Navigation.Settings.name} screen !")
                        }
                    }
                }
            }
        }
    }
}


@Composable
@Preview
fun MainScreenPreview() {
    MainScreen()
}

