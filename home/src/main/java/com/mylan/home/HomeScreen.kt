package com.mylan.home

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.tooling.preview.Preview
import com.mylan.core.Navigation
import com.mylan.core.ui.theme.MyApplicationTheme

@Composable
fun HomeScreen(onClick : () -> Unit) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = Navigation.Home.name)
        Button(onClick = onClick) {
            Text(text = "Click me")
        }
    }
}


@Composable
@Preview
fun HomeScreenPreview() {
    MyApplicationTheme {
        HomeScreen(onClick = {})
    }
}